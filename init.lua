toggle = 0
pin = 7

gpio.mode(pin, gpio.OUTPUT)

print("starting server...")

server=net.createServer(net.TCP) 
server:listen(80,function(connection)
  connection:on("receive", function(c, payload) 
     print(payload) 
     local _, _, method, path, vars = string.find(payload, "([A-Z]+) (.+)?(.+) HTTP");
     if(method == nil)then
        _, _, method, path = string.find(payload, "([A-Z]+) (.+) HTTP");
     end
     
     print("method " .. method)
     print("path " .. path)
     if(vars ~= nil)then
        print("vars " .. vars)
     end

    if(string.find(path, "/status") ~= nil)then
        sendJson(connection, '{"items":[{"name":"Node 1","status":' .. toggle .. ',"order":1}]}')
    elseif(string.find(path, "/1/on") ~= nil)then
        toggle = 1
		gpio.write(pin,gpio.LOW)
        print("Pin Value " .. gpio.read(pin))
		sendJson(connection, '{"items":[{"name":"Node 1","status":' .. toggle .. ',"order":1}]}')
    elseif(string.find(path, "/1/off") ~= nil)then
        toggle = 0
		gpio.write(pin,gpio.HIGH)
        print("Pin Value " .. gpio.read(pin))
		sendJson(connection, '{"items":[{"name":"Node 1","status":' .. toggle .. ',"order":1}]}')
    else
        connection:send("hello world")
        connection:close() 
    end
  end)
end)

function sendJson(connection, json)
    connection:send("HTTP/1.1 200 OK\r\n")
    connection:send("Content-Type: application/json\r\n")
    connection:send("Content-Length" .. tostring(string.len(json)) .. "\r\n")
    connection:send("\r\n")
    connection:send(json)
    connection:close()
end
    
